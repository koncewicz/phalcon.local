Web server:

https://box.scotch.io


Install phalcon:

1. $ curl -s "https://packagecloud.io/install/repositories/phalcon/stable/script.deb.sh" | sudo bash
2. $ sudo apt-get install php7.0-phalcon
3. $ service apache2 restart


Install app:

1. Add files to /var/www

2. Import sql/dump.sql

3. URL to app: http://192.168.33.10


Log in:

login: admin@admin.adm

pass: admin
