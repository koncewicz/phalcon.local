CREATE TABLE `users` (
    `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    `email` VARCHAR(70) NOT NULL,
    `password` VARCHAR(200) NOT NULL,
    PRIMARY KEY (`id`)
);

INSERT INTO `users` (`email`, `password`) VALUES ('admin@admin.adm', '$2y$08$YUpVdExUNU1MVHVMQUp6TuxfKCGqg/2WS7y7/bynQjLOVXD1btnO2');
