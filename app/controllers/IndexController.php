<?php

use Phalcon\Mvc\Controller;

class IndexController extends Controller
{
    public function indexAction()
    {
        $this->assets->addCss('css/main.css');

        $sessions = $this->getDI()->getShared("session");
        if ($sessions->has("user_id") && $sessions->get("user_id")) {
            return $this->response->redirect("data/index");
        }
    }


}
