<?php

use Phalcon\Mvc\Controller;

class LogoutController extends Controller
{
    public function indexAction()
    {
        $sessions = $this->getDI()->getShared("session");
        $sessions->set("user_id", null);
        return $this->response->redirect("/");
    }
}
