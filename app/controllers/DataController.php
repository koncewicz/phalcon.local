<?php

use Phalcon\Mvc\Controller;
use Phalcon\Mvc\Url;

class DataController extends Controller
{
    public function indexAction()
    {
        $this->assets->addCss('css/main.css');
        $this->assets->addCss('css/jquery.dataTables.min.css');

        $this->assets->addJs('js/jquery-3.3.1.min.js');
        $this->assets->addJs('js/jquery.dataTables.min.js');
        $this->assets->addJs('js/dataTables.pageLoadMore.min.js');

        $sessions = $this->getDI()->getShared("session");
        if (!$sessions->has("user_id") || !$sessions->get("user_id")) {
            return $this->response->redirect("/");
        }

        $text = $this->request->get("text");
        $this->view->text = $text;
        $this->view->showTable = false;

        if ($text) {
            $this->view->showTable = true;
        }
    }

    public function ajaxAction()
    {
        $this->view->disable();

        $draw = $this->request->get("draw");
        if (!$draw) {
            $draw = 1;
        }

        $sessions = $this->getDI()->getShared("session");
        if ($draw == 1) {
            $sessions->set('g_token', '');
        }

        $token = null;
        $tokenUrl = '';

        if ($sessions->has("g_token") && $sessions->get("g_token")) {
            $token = $sessions->get("g_token");
            $tokenUrl = '&pagetoken=' . $token;
        }

        $text = $this->request->get("text");
        if ($text) {
            $url = 'https://maps.googleapis.com/maps/api/place/textsearch/json?key=AIzaSyDyIR6nVkPnGLUrb4A9-eDKNQSaZ9aE3OI&input=' . urlencode($text) . '&inputtype=textquery&fields=photos,formatted_address,name,rating,opening_hours,geometry' . $tokenUrl;

            $data = file_get_contents($url);
            $json = json_decode($data);

            $sessions->set('g_token', $json->next_page_token);

            $items = [];
            foreach ($json->results as $val) {
                $items[] = [$val->name, $val->formatted_address];
            }

            $recordsFiltered = 10000;
            if (!$json->next_page_token) {
                $recordsFiltered = 0;
            }

            $data = [
                "draw" => $draw,
                "recordsFiltered" => $recordsFiltered,
                "data" => $items
            ];
        } else {
            $data = [
                "draw" => $draw,
                "recordsFiltered" => 11,
                "data" => []
            ];
        }

        $response = new \Phalcon\Http\Response();
        $response->setContent(json_encode($data));

        return $response;
    }
}
