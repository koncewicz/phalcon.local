<?php

use Phalcon\Mvc\Controller;

class LoginController extends Controller
{
    public function indexAction()
    {
        $sessions = $this->getDI()->getShared("session");

        if ($sessions->has("user_id") && $sessions->get("user_id")) {
            return $this->response->redirect("data/index");
        }

        if ($this->request->isPost()) {

            $password = $this->request->getPost("password");
            $email = $this->request->getPost("email");

            if (!$email) {
                $this->flashSession->error("Please enter your email");
                return $this->response->redirect('index/index');
            }

            if (!$password) {
                $this->flashSession->error("Please enter your password");
                return $this->response->redirect('index/index');
            }

            $user = Users::findFirst([
                "email = ?0",
                "bind" => [$email]
            ]);

            if ($user !== false && $this->security->checkHash($password, $user->password)) {
                $sessions->set("user_id", $user->id);
                return $this->response->redirect('data/index');
            }

            $this->flashSession->error("Email or password is wrong");
            return $this->response->redirect("/");
        }
    }
}
